# 1 bod
# z retezce string1 vyberte pouze prvni tri slova a pridejte za ne tri tecky. Vypiste je
# spravny vypis: There are more...
string1 = 'There are more things in Heaven and Earth, Horatio, than are dreamt of in your philosophy.'

prvnitri= (string1.split(' '))

print(prvnitri[0] + ' ' + prvnitri[1] + ' ' + prvnitri[2] + '...')


# 1 bod
# z retezce string1 vyberte pouze poslednich 11 znaku a vypiste je
# spravny vypis: philosophy.

print(string1[-11:])


# 1 bod
# retezec string2 rozdelte podle carek a jednotlive casti vypiste
# spravny vypis:
# I wondered if that was how forgiveness budded; not with the fanfare of epiphany
# but with pain gathering its things
# packing up
# and slipping away unannounced in the middle of the night.
string2 = 'I wondered if that was how forgiveness budded; not with the fanfare of epiphany, but with pain gathering its things, packing up, and slipping away unannounced in the middle of the night.'

carka1 = string2.split(',')
for carka1 in carka1:
    print(carka1)

# 2 body
# v retezci string2 spocitejte cetnost jednotlivych pismen a pote vypiste jednotlive cetnosti i pismenem
# spravny vypis: a: 12
#                b: 2
# atd.

cetnost = {}
for i in range(97, 123):
    cetnost[chr(i)] = string2.lower().count(chr(i))

for letter, cetnost in cetnost.items():
    print(letter + ': ' + str(cetnost))

# 5 bodu
# retezec datetime1 predstavuje datum a cas ve formatu YYYYMMDDHHMMSS
# zparsujte dany retezec (rozdelte ho na rok, mesic, den, hodiny, minuty, sekundy a vytvorte z nej datum tak jak definuje
# funkce datetime. Pouzijete knihovnu a tridu datetime (https://docs.python.org/3/library/datetime.html#datetime.datetime)
# Potom slovy odpovezte na otazku: Co je to cas od epochy (UNIX epoch time)?
# Odpoved:
# Nasledne odectete zparsovany datum a cas od aktualniho casu (casu ziskaneho z pocitace pri zavolani jiste funkce)
# a vypocitejte kolik hodin cini rozdil a ten vypiste
datetime1 = '20181121191930'

import datetime



rok = int(datetime1[:4])
mesic = int(datetime1[4:6])
den = int(datetime1[6:8])
hodina = int(datetime1[8:10])
minuta = int(datetime1[10:12])
vterina = int(datetime1[12:])

datetime2 = datetime.datetime(rok, mesic, den, hodina, minuta, vterina)
time = datetime2.today()

rozdil = (time - datetime2).days * 24 + (time - datetime2).seconds / 3600

print(rozdil)

# 5 bodu
# v retezci string3 jsou slova prehazena. V promenne correct_string3 jsou slova spravne.
# vytvorte novou promennou, do ktere poskladate spravne retezec string3 podle retezce correct_string3
# nasledne vypocitej posun mist o ktere muselo byt slovo posunuto, aby bylo dano do spravne pozice.
# nereste pritom smer pohybu
# vypiste spravne poradi vety a jednotliva slova s jejich posunem
# napr. war: 8
#       the: 6
# atd.
string3 = 'war concerned itself with which Ministry The of Peace'
correct_string3 = 'The Ministry of Peace which concerned itself with war'



spatne = string3.split(' ')
oprava = [None in range(len(spatne))]
lags = {}

for x in spatne:
    index = correct_string3.split(' ').index(x)
    oprava[index] = x
    lags[x] = abs(spatne.index(x) - index)

    print((' ').join(oprava))

for x, lag in lags.items():

    print(x + ': ' + str(lag))



